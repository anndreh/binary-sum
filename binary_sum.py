def sum(a,b):
    list_a = []
    list_b = []

    result_number = list()
    for digit in str(a):
        list_a.append(int(digit))
    for digit in str(b):
        list_b.append(int(digit))
    list_a.reverse()
    list_b.reverse()

    if len(list_a) >= len(list_b):
        sum_elements = 0
        pos = 0
        spare = 0
        for elem in list_a:
            value_list_b = list_b[pos] if pos < len(list_b) else 0
            sum_elements = elem + value_list_b + spare
            spare = 0
            if sum_elements == 3:
                spare = 1
                sum_elements = 1
            elif sum_elements == 2:
                spare = 1
                sum_elements = 0
            result_number.append(sum_elements)
            pos += 1
        if spare == 1:
            result_number.append(1)
    else:
        sum_elements = 0
        pos = 0
        spare = 0
        for elem in list_b:
            value_list_a = list_a[pos] if pos < len(list_a) else 0
            sum_elements = elem + value_list_a + spare
            spare = 0
            if sum_elements == 3:
                spare = 1
                sum_elements = 1
            elif sum_elements == 2:
                spare = 1
                sum_elements = 0
            result_number.append(sum_elements)
            pos += 1
        if spare == 1:
            result_number.append(1)

    result_number.reverse()
    return int(''.join(map(str,result_number)))
